import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnepageComponent } from './component/CheckoutModule/onepage/onepage.component';
import { PaymentComponent } from './component/CheckoutModule/payment/payment.component';
import { CartComponent } from './component/CheckoutModule/cart/cart.component';
import { BusinessLandingPageComponent } from './component/BusinessInquiryModule/business-landing-page/business-landing-page.component';
import { InquiryFormComponent } from './component/BusinessInquiryModule/inquiry-form/inquiry-form.component';

const routes: Routes = [
  {path: 'checkout',  component: OnepageComponent},
  {path:'payment', component: PaymentComponent},
  {path:'cart', component: CartComponent},
  {path: 'business', children:[
    {
      path: 'landing', component: BusinessLandingPageComponent,
    },
    {
      path: 'inquiry', component: InquiryFormComponent,
    },
    {
      path:'**',
      redirectTo:'landing',
      pathMatch:'full'
    }
  ]},
  { path: '**',
    redirectTo: 'cart',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
