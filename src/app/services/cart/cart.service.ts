import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  base_url="https://wia.id/wia-module/checkout"
  constructor(private http: HttpClient) { }

  removeItem(itemId: String):Observable<any>{
    return this.http.get(`${this.base_url}/deleteproduct?product=${itemId}`)
  }

  update(itemId: String, newQty: any):Observable<any>{
    return this.http.get(`${this.base_url}/updatecart?product=${itemId}&qty=${newQty}`)
  }
}
