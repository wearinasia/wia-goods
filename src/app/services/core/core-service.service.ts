import { Injectable, OnInit, Renderer, RendererFactory2, Renderer2 } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { MatDialogConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})

export class CoreServiceService {
  _deviceInfo = null;
  isMobile:Boolean
  isTablet:Boolean
  isDesktop:Boolean
  private renderer: Renderer2;

  constructor(
    public deviceService: DeviceDetectorService,
    private rendererFactory: RendererFactory2,
  ) { 
    this.renderer = rendererFactory.createRenderer(null, null);
    this._deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    this.isTablet = this.deviceService.isTablet();
    this.isDesktop = this.deviceService.isDesktop();
  }

  dialogConfig(data?: {}){
    let _dialogConfig = new MatDialogConfig()

    _dialogConfig.disableClose = true
    _dialogConfig.autoFocus = false

    if(this.isDesktop || this.isTablet){
      _dialogConfig.height = '80vh'
      _dialogConfig.minWidth='60vw'
      _dialogConfig.maxWidth='60vw'
      _dialogConfig.panelClass=['mat-dialog-mobile']
    }else{
      _dialogConfig.height = '100vh'
      _dialogConfig.minWidth='100vw'
      _dialogConfig.panelClass=['mat-dialog-mobile','medium-grey-bg']
    }
    if(data != null) _dialogConfig.data=data
    return _dialogConfig
  }

  disableScrollBody(){
    if(this.isMobile) this.renderer.setAttribute(document.body,'class', 'no-scroll');
  }

  enableScrollBody(){
    if(this.isMobile) this.renderer.removeAttribute(document.body, 'class')
  }

}
