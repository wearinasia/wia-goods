import { Injectable } from '@angular/core';
import { Item } from 'src/app/model/item';
declare var gtag: any
declare var fbq: any

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor() { }

  beginCheckout(cart: Item[]){
    console.log('begin_checkout_tag')
    var cartTrack: any = cart
    gtag('event', 'begin_checkout', {
      items: cartTrack
    })

    console.log('begin_fb_checkout')
    var subtotal = 0
    cartTrack.forEach((item: Item)=>{
      subtotal+=item.quantity*parseInt(item.price)
    })
    fbq('track', 'InitiateCheckout',{
      value: subtotal+'.00',
      currency: 'IDR',
      contents: cartTrack.map((item: Item) =>{
        return  {
          'id': item.id, //product id
          'quantity': item.quantity
        }
      }),
      content_type: 'product'
    })

  }

  beginPayment(cart: Item[]){
    console.log('begin_payment')
    var cartTrack: any = cart

    console.log(cartTrack)
    gtag('event', 'checkout_progress', {
      "items": cartTrack,
      "checkout_step": 1,
    });

    console.log('begin_payment_fb')
    var subtotal = 0
    cartTrack.forEach((item: Item)=>{
      subtotal+=item.quantity*parseInt(item.price)
    })
    fbq('track', 'PaymentPage',{
      value: subtotal+'.00',
      currency: 'IDR',
      contents: cartTrack.map((item: Item) =>{
        return  {
          'id': item.id, //product id
          'quantity': item.quantity
        }
      }),
      content_type: 'product'
    })
  }

}
