import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Address } from 'src/app/model/address';
import { Customer } from 'src/app/model/customer';
import { Item } from 'src/app/model/item';
import { ShippingMethod } from 'src/app/model/shippingMethod';

export interface Quote{
  address: Address
  code: any
  customer: Customer
  id: any
  message_code: any
  message_dialog: any
  promo: any
  quote: Item[]
  shipping: any
  shipping_list: ShippingMethod[]
  shipping_name: any
  subtotal: any
  total: any
}

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  private baseUrl= 'https://wia.id/wia-module/checkoutv2'
    constructor(private http: HttpClient){}

    setSelectedAddress(address: Address):Observable<any>{
      console.log(this.baseUrl+'/address?id='+address.id)
      return this.http.post(this.baseUrl+'/address?id='+address.id,
      {})
    }
    
    getAddresses():Observable<any>{
      return this.http.get(this.baseUrl+'/address');
    }
    
    addNewAddress(body:{}):Observable<any>{
      return this.http.post(
        this.baseUrl+'/address',
        body
        );
    }

    editAddress(body:Address):Observable<any>{
      return this.http.put(
        this.baseUrl+'/address?id='+body.id,
        body,
      )
    }
    
    getQuote():Observable<any>{
      return this.http.get(this.baseUrl+'/quote');
    }

    getRegions(countryID: String):Observable<any>{
      return this.http.get(this.baseUrl+'/regionlist?country='+countryID)
    }

    getCities(regionID: String):Observable<any>{
      return this.http.get(this.baseUrl+'/citylist?region='+regionID)
    }

    getVouchers():Observable<any>{
      return this.http.get(this.baseUrl+'/voucher')
    }

    useVoucher(voucherCode: String):Observable<any>{
      return this.http.post(
        this.baseUrl+'/voucher?code='+voucherCode,
        {}
      )
    }

    setShippingMethod(shippingCode: String):Observable<any>{
      return this.http.post(
        this.baseUrl+'/setshipping?code='+shippingCode,
        {}
      )
    }

    createOrder(recipient?: String, message?: String):Observable<any>{
      if(recipient != null && message != null){
        console.log(recipient)
        console.log(message)
        return this.http.post(
          this.baseUrl+`/createorder`,
          {
            recepient: recipient,
            message: message
          }
        )
      }
      else return this.http.get(
        this.baseUrl+'/createorder',
      )
    }


}
