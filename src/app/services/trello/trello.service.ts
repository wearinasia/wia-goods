import { Injectable } from '@angular/core';
import { UrlSerializer, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrelloService {
  private kBaseURL = "https://api.trello.com/1/"

  private kIDList="5be297be7f0bd106f3b9333c"
  private kKey="700d34827dc3666a88acc666bae68806"
  private kToken="55b8c2da719c3f8e28b97eed0d10b0e7b9f70ecc92ae0d3cffc44b90e9aed8ae"
  private kIdMembers="532d5415f6a167117d8d3180,547c4be5b5a29b3b999c01d7,5be274cf3953ab081c172e74"
  private kIdLabels="5be2956aa724a908c0fc020a"

  constructor(
    private serializer: UrlSerializer,
    private router: Router,
    private http: HttpClient
    ) { }

  doPush(name: any, desc: any, due: any, source?: any): Observable<any>{
    var urlRaw = this.router.createUrlTree(['/'],{
      queryParams: {
        idList: this.kIDList,
        keepFromSource: 'all',
        key: this.kKey,
        token: this.kToken,
        name: name,
        desc: desc,
        idMembers: this.kIdMembers,
        idLabels: this.kIdLabels,
        due: due,
      }
    })
    var url = this.kBaseURL+'cards'+this.serializer.serialize(urlRaw)
    console.log(url)
    return this.http.post(url,{})
  }
}
