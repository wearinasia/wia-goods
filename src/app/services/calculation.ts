export abstract class Calculation{
    static subtotal: any = 0
    static shipping: any = 0
    static promo: any = 0
    static total: any = 0

    static recalculate(){
        this.total = Number(this.subtotal) + Number(this.shipping) - Number(this.promo)
    }

    static changeShipping(newShip: any){
        this.shipping = Number(newShip)
        this.recalculate();
    }

    static changePromo(newPromo: any){
        this.promo = Number(newPromo)
        this.recalculate();
    }

    static isEmpty(){
        return (this.subtotal == null && this.shipping == null && this.promo == null && this.total == null)
    }
}
