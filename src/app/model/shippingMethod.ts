export interface ShippingMethod{
    "code": String,
    "carrier": String,
    "carrier_title": String,
    "method": String,
    "method_title": String,
    "method_description": String,
    "price": Number,
    "carrierName": String,
}