export interface Promo{
    code: String,
    description: String,
    discount: Number,
}