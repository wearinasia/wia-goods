export interface Address{
    city: String,
    company: String,
    country_id: String,
    customer_id: String,
    location: String,
    email: String,
    firstname: String,
    id: String,
    lastname: String,
    postcode: String,
    region: String,
    region_id: Number,
    street: String,
    telephone: String,
    vat_id: any
}

export function isAddressSame(source: Address,dest: Address){
    return (
      source.city == dest.city &&
      source.country_id == dest.country_id &&
      source.customer_id == dest.customer_id &&
      source.location == dest.location &&
      source.email == dest.email &&
      source.firstname == dest.firstname &&
      source.lastname == dest.lastname &&
      source.postcode == dest.postcode &&
      source.region == dest.region &&
      source.street == dest.street &&
      source.telephone == dest.telephone &&
      source.vat_id == dest.vat_id
    )
  }
