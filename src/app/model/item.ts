export interface Item{
    id: string,
    name: string,
    price: string,
    quantity: any,
    product_quantity: any,
    small_image: string,
    brand: string,
    category: string
}
