export interface Region{
    region_id: String,
    country_id: String,
    code: String,
    default_name: String,
    name: String
}