import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CoreServiceService } from 'src/app/services/core/core-service.service';

@Component({
  selector: 'app-business-landing-page',
  templateUrl: './business-landing-page.component.html',
  styleUrls: ['./business-landing-page.component.scss']
})
export class BusinessLandingPageComponent implements OnInit {

  constructor(
    private titleService: Title,
    public core: CoreServiceService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle("Business Solution - Landing Page")
  }

  interact(e: String){
    if(e.match('leading')){
      location.href="https://wia.id/"
    }
    if(e.match('trailing')){
      this.openLiveChat()
    }
  }

  openLiveChat(){
  }
  

}
