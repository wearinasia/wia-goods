import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessLandingPageComponent } from './business-landing-page.component';

describe('BusinessLandingPageComponent', () => {
  let component: BusinessLandingPageComponent;
  let fixture: ComponentFixture<BusinessLandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessLandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
