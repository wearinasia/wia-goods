import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';
import { TrelloService } from 'src/app/services/trello/trello.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { CoreServiceService } from 'src/app/services/core/core-service.service';

@Component({
  selector: 'app-inquiry-form',
  templateUrl: './inquiry-form.component.html',
  styleUrls: ['./inquiry-form.component.scss']
})
export class InquiryFormComponent implements OnInit {
  isSending: Boolean

  minDate = new Date()
  maxDate = new Date(this.minDate.getFullYear()+2, 0, 1);
  
  inquryForm = new FormGroup({
    name: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
    companyName: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
    companyAddress: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
    companyEmail: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.email, Validators.required]
    }),
    handphone: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
    product: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
    dueDate: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
  })

  constructor(
    private snackbar: MatSnackBar,
    private trelloSvc: TrelloService,
    private router: Router,
    private titleService: Title,
    public core: CoreServiceService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle("Business Solution - Inquiry")
    this.isSending=false
  }

  onSubmit(){
    if(this.inquryForm.valid){
      this.isSending =true
      var submit = this.inquryForm.value
      submit.dueDate = submit.dueDate.toISOString()

      var date=new Date()

      var title=date.getDate()+"/"+date.getMonth()+" "+submit.name+` - ${submit.companyName}`+" ("+submit.companyEmail+")"
      var body = `Telp: ${submit.handphone}\n\nAddress:${submit.companyAddress}\n\n${submit.product}`
      this.trelloSvc.doPush(title, body, submit.dueDate).subscribe((response)=>{
        this.isSending=false
        if(response.hasOwnProperty('id')){
          this.router.navigate(['/','business'])
          this.snackbar.open(
            "Thank you for your inquiry. We will reply you back as fast as possible..",
            null,
            createSnackBarConfig("toast_success",4000)
          )
        }else{
          this.snackbar.open(
            "Please try to submit the inquiry again",
            null,
            createSnackBarConfig("toast_danger")
          )
        }
      })
    }else{
      this.inquryForm.markAllAsTouched()
      this.snackbar.open(
        "Please complete this form to continue",
        null,
        createSnackBarConfig(
          "toast_danger"
        )
      )
    }
  }

  interact(e: String){
    if(e.match('leading')){
      this.router.navigate(['/','business','landing'])
    }
    if(e.match('trailing')){
      this.openLiveChat()
    }
  }

  openLiveChat(){
  }
  
}
