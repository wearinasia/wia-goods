import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Item } from 'src/app/model/item';
import { Calculation } from 'src/app/services/calculation';

@Component({
  selector: 'app-summary-detail',
  templateUrl: './summary-detail.component.html',
  styleUrls: ['./summary-detail.component.scss']
})
export class SummaryDetailComponent implements OnInit {
  public calc = Calculation

  constructor(
    private dialogRef: MatDialogRef<SummaryDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public cart: Item[]
  ) { }

  ngOnInit() {}

  interact(e: string){
    if(e.match('title')){
      
    }else if(e.match('trailing')){
      this.dialogRef.close({refresh: true});
    }
  }

}
