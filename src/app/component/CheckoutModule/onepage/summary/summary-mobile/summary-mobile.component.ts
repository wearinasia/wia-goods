import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'src/app/model/item';
import { SummaryDetailComponent } from './summary-detail/summary-detail.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { Calculation } from 'src/app/services/calculation';
import { faChevronDown} from '@fortawesome/free-solid-svg-icons'
import { Quote } from 'src/app/services/checkout/checkout.service';

@Component({
  selector: 'app-summary-mobile',
  templateUrl: './summary-mobile.component.html',
  styleUrls: ['./summary-mobile.component.scss']
})
export class SummaryMobileComponent implements OnInit {
  @Input() quote: Quote

  cart: Item[]
  chevronDown = faChevronDown

  public calc = Calculation

  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnInit() {

    this.cart = this.quote.quote
  }

  openSummaryDetails(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true

    dialogConfig.height = '100vh'
    dialogConfig.minWidth ='100vw'
    dialogConfig.panelClass='mat-dialog-mobile'
    dialogConfig.data=this.cart
    
    let dia = this.dialog.open(SummaryDetailComponent, dialogConfig);
    dia.afterClosed().subscribe(
      (f)=> {}
    )
  }

  getTotalItems(){
    var amount = 0;
    this.cart.forEach((item)=>{
      amount+=item.quantity
    })
    return amount
  }

}
