import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'src/app/model/item';
import {Calculation} from 'src/app/services/calculation'
import { Quote } from 'src/app/services/checkout/checkout.service';

@Component({
  selector: 'app-summary-desktop',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  @Input() quote: Quote

  cart: Item[]

  subTotal: Number
  shipping:Number
  promo:Number
  grandTotal: Number

  public calc = Calculation

  constructor(
  ) { }

  ngOnInit() {
    this.cart = this.quote.quote
  }


}
