import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'src/app/model/item';
import { Router } from '@angular/router';
import { TagService } from 'src/app/services/tag/tag.service';
import { Customer } from 'src/app/model/customer';
import { Quote } from 'src/app/services/checkout/checkout.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  @Input() quote: Quote
  cart: Item[]
  customer: Customer

  constructor(
    private router: Router,
    private tagSvc: TagService
  ) {}

  ngOnInit() {
    console.log("Account init")
    console.log(this.quote)
    switch(this.quote.message_code){
      case 200:{
        this.cart = this.quote.quote
        this.tagSvc.beginCheckout(this.cart)
        this.customer = this.quote.customer
        break;
      }
      case 400:{
        this.router.navigate(['/','cart'])
        break;
      }
    }
  }
}
