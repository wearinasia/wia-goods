import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAsGiftComponent } from './send-as-gift.component';

describe('SendAsGiftComponent', () => {
  let component: SendAsGiftComponent;
  let fixture: ComponentFixture<SendAsGiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendAsGiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAsGiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
