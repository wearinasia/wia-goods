import { Component, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-send-as-gift',
  templateUrl: './send-as-gift.component.html',
  styleUrls: ['./send-as-gift.component.scss']
})
export class SendAsGiftComponent implements OnInit, OnChanges{
  @Output() sendAsGiftDataListener = new EventEmitter

  emit(){
    this.sendAsGiftDataListener.emit({
      enabled : this.sendAsGift,
      valid: this.giftForm.valid,
      recipient : this.giftForm.get('recipient').value,
      message : this.giftForm.get('message').value
    })
  }

  toggleSendAsGift(){
    if(!this.sendAsGift) this.giftForm.reset()
    this.emit()
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(changes)
    if(changes.hasOwnProperty('sendAsGift')){
      this.sendAsGiftDataListener.emit({
        enabled : changes.sendAsGift.currentValue,
        valid: this.giftForm.valid,
        recipient : this.giftForm.get('recipient').value,
        message : this.giftForm.get('message').value
      })
    }
  }

  sendAsGift: Boolean

  giftForm = new FormGroup({
    recipient: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
    message: new FormControl(null,{
      updateOn: "change",
      validators: [Validators.required]
    }),
  })
  
  constructor() { }

  ngOnInit() {
    this.sendAsGift=false

    this.giftForm.valueChanges.subscribe(
      (res)=>{
        this.emit()
      }
    )
  }

}
