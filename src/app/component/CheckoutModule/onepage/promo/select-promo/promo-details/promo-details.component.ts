import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Promo } from 'src/app/model/promo';

@Component({
  selector: 'app-promo-details',
  templateUrl: './promo-details.component.html',
  styleUrls: ['./promo-details.component.scss']
})
export class PromoDetailsComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<PromoDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Promo
  ) { }

  ngOnInit() {
  }

  closeDialog(){
    this.dialogRef.close();
  }

  useThisPromo(){
    this.dialogRef.close();
  }

}
