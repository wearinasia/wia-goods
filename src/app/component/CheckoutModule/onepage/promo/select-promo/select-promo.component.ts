import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { PromoDetailsComponent } from './promo-details/promo-details.component';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';
import { Promo } from 'src/app/model/promo';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { Calculation } from 'src/app/services/calculation';

@Component({
  selector: 'app-select-promo',
  templateUrl: './select-promo.component.html',
  styleUrls: ['./select-promo.component.scss']
})
export class SelectPromoComponent implements OnInit, OnDestroy {
  selectedVoucher: String
  vouchers: Promo[]

  public calc = Calculation

  constructor(
    private checkoutSvc: CheckoutService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<PromoDetailsComponent>,
    private snackBar: MatSnackBar,
    public core: CoreServiceService,
  ) { }

  ngOnInit() {
    this.selectedVoucher=""
    this.core.disableScrollBody();
    this.checkoutSvc.getVouchers().subscribe(
      (res)=>{
        if(res.message_code == 200){
          for(let nVoc in res.promo){
            if(this.vouchers == null) this.vouchers=[]
            this.vouchers.push(res.promo[nVoc])
          }
        }
      }
    )
  }

  ngOnDestroy(){
    this.core.enableScrollBody();
  }

  openPromoDetails(i: any){
    let dia = this.dialog.open(PromoDetailsComponent, this.core.dialogConfig(
      this.vouchers[i]
    ));
    dia.afterClosed().subscribe(
      (f)=> {}
    )
  }

  copyVoucher(i: String){
    this.selectedVoucher = i
    this.applyPromo();
  }

  applyPromo(){
    this.checkoutSvc.useVoucher(this.selectedVoucher).subscribe((response)=>{
      if(response.message_code == 200){
        if(response.promo > 0){
          this.snackBar.open(
            response.message_dialog,
            null,
            createSnackBarConfig('toast_success', 2000)
          )
          this.calc.changePromo(response.promo)

          this.dialogRef.close(
            {success: true}
          )

        }else{
          this.calc.changePromo(0)
          this.snackBar.open(
            'This promo cannot be applied. Please try another',
            null,
            createSnackBarConfig('toast_danger', 2000)
          )

          this.dialogRef.close(
            {success: false}
          )
        }
      }
    })
  }

  interact(e: string){
    if(e.match('title')){
      
    }else if(e.match('trailing')){
      this.dialogRef.close();
    }
  }
}
