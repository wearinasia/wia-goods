import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CheckoutService, Quote } from 'src/app/services/checkout/checkout.service';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { SelectPromoComponent } from '../select-promo/select-promo.component';
import { Promo } from 'src/app/model/promo';
import { faTag } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-promo-desktop',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.scss']
})
export class PromoComponent implements OnInit {
  selectedVoucher: Promo
  isPromoApplied: Boolean
  promos: Promo[]
  icon_tag

  @Input() quote: Quote
  
  constructor(
    private checkoutSvc: CheckoutService,
    private dialog: MatDialog,
    private core: CoreServiceService,
  ) { }

  ngOnInit() {
    this.icon_tag = faTag;
    this.checkoutSvc.getQuote().subscribe(
      (res)=>{
        if(res.message_code == 200){
          if(res.promo > 0) this.isPromoApplied=true
          else this.isPromoApplied=false
        }
      }
    )
    this.checkoutSvc.getVouchers().subscribe(
      (res)=>{
        if(res.message_code == 200){
          this.promos = res.promo
        }else{
          this.promos=[]
        }
      }
    )
  }

  openSelectPromo(){
    let dia = this.dialog.open(SelectPromoComponent, this.core.dialogConfig());
    dia.afterClosed().subscribe(
      (res)=>{
       if(res != null){
          if(res.success) this.isPromoApplied=true
          else this.isPromoApplied=false
        }
      })
  }

}
