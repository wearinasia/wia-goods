import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoMobileComponent } from './promo-mobile.component';

describe('PromoMobileComponent', () => {
  let component: PromoMobileComponent;
  let fixture: ComponentFixture<PromoMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
