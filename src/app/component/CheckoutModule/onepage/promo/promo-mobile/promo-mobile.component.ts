import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CheckoutService, Quote } from 'src/app/services/checkout/checkout.service';
import { SelectPromoComponent } from '../select-promo/select-promo.component';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { Promo } from 'src/app/model/promo';

import {faTag} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-promo-mobile',
  templateUrl: './promo-mobile.component.html',
  styleUrls: ['./promo-mobile.component.scss']
})
export class PromoMobileComponent implements OnInit {
  selectedVoucher: Promo
  isPromoApplied: Boolean
  promos: Promo[]

  @Input() quote: Quote
  
  icon_tag

  constructor(
    private checkoutSvc: CheckoutService,
    private dialog: MatDialog,
    private core: CoreServiceService,
  ) { }

    ngOnInit() {
    this.icon_tag = faTag;

    if(this.quote.promo > 0) this.isPromoApplied=true
    else this.isPromoApplied=false

    this.checkoutSvc.getVouchers().subscribe(
      (res)=>{
        if(res.message_code == 200){
          this.promos = res.promo
        }else{
          this.promos=[]
        }
      }
    )
  }

  openSelectPromo(){    
    let dia = this.dialog.open(SelectPromoComponent, this.core.dialogConfig());
    dia.afterClosed().subscribe(
      (res)=>{
        if(res != null){
          if(res.success) this.isPromoApplied=true
          else this.isPromoApplied=false
         }
      })
  }
}
