import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectShippingMethodComponent } from './select-shipping-method.component';

describe('SelectShippingMethodComponent', () => {
  let component: SelectShippingMethodComponent;
  let fixture: ComponentFixture<SelectShippingMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectShippingMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectShippingMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
