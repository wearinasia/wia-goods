import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ShippingMethod } from 'src/app/model/shippingMethod';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';

@Component({
  selector: 'app-select-shipping-method',
  templateUrl: './select-shipping-method.component.html',
  styleUrls: ['./select-shipping-method.component.scss']
})
export class SelectShippingMethodComponent implements OnInit, OnDestroy{
  constructor(
    private dialogRef: MatDialogRef<SelectShippingMethodComponent>,
    public core: CoreServiceService,
    @Inject(MAT_DIALOG_DATA) public data: {
      selected: ShippingMethod,
      available: ShippingMethod[]
    }
    ) { }

  ngOnInit() {
    console.log(this.data)
    //happens when user changes address, so the selected shipping method will be null
    if(this.data.selected == null){
      this.data.selected={
        "code": null,
        "carrier": null,
        "carrier_title": null,
        "method": null,
        "method_title": null,
        "method_description": null,
        "price": null,
        "carrierName": null,
      }
    }

    this.core.disableScrollBody();

  }

  ngOnDestroy(): void {
    this.core.enableScrollBody();
  }

  useThisShippingMethod(index){
    this.dialogRef.close(this.data.available[index])
  }

  interact(e: string){
    if(e.match('title')){
      
    }else if(e.match('trailing')){
      this.dialogRef.close()
    }
  }

}
