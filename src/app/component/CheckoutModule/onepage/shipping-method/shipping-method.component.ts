import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import {MatDialog, MatDialogRef, MatSnackBar} from "@angular/material";
import { SelectShippingMethodComponent } from './select-shipping-method/select-shipping-method.component';
import { CheckoutService, Quote } from 'src/app/services/checkout/checkout.service';
import { ShippingMethod } from 'src/app/model/shippingMethod';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { Calculation } from 'src/app/services/calculation';
import { faChevronRight} from '@fortawesome/free-solid-svg-icons'
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';

@Component({
  selector: 'app-shipping-method',
  templateUrl: './shipping-method.component.html',
  styleUrls: ['./shipping-method.component.scss']
})

export class ShippingMethodComponent implements OnInit, OnChanges {
  @Output() isStatisfied = new EventEmitter()
  @Input() resetShippingMethod: Boolean
  @Input() isShippingAddressValid: Boolean
  @Input() quote: Quote

  chevronRight= faChevronRight
  
  shippingMethod: ShippingMethod[]
  selectedShippingMethod: ShippingMethod

  public calc = Calculation
  
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    
    if(changes.hasOwnProperty('isShippingAddressValid')){
      this.isShippingAddressValid = changes.isShippingAddressValid.currentValue
      //if shipping address is just been valid..
      if(changes.isShippingAddressValid.previousValue == !changes.isShippingAddressValid.currentValue){
        this.ngOnInit()
      }
    }
      

    if(changes.hasOwnProperty('resetShippingMethod')){
      //reset shipping method
      if(!changes.resetShippingMethod.currentValue) {
        this.calc.changeShipping(0)
        this.selectedShippingMethod=null
      }
    }
  }

  constructor(
    private checkoutSvc: CheckoutService,
    private dialog: MatDialog,
    public core: CoreServiceService,
    private snackbar: MatSnackBar,
    ) {}

  ngOnInit() {
    if(this.quote.message_code == 200){
      // whatever the shipping code doesn't describe which shipping method is selected, assumed
      // the shipping method has not been selected yet
      if(this.quote.code == null || this.quote.code == "") {
        this.selectedShippingMethod = null
        this.calc.changeShipping(0)
        this.isStatisfied.emit('false')
      }    
      else {
        this.calc.changeShipping(this.quote.shipping)
        this.selectedShippingMethod = {
            "code": this.quote.code,
            "carrier": null,
            "carrier_title": null,
            "method": null,
            "method_title": this.quote.shipping_name,
            "method_description": null,
            "price": this.quote.shipping,
            "carrierName": null,
        }
        this.isStatisfied.emit('true')
      }
    }
  }

  openSelectShippingMethod(){    
    
    let dia = this.dialog.open(SelectShippingMethodComponent, this.core.dialogConfig({
      selected: this.selectedShippingMethod,
      available: this.quote.shipping_list
    }));

    dia.afterClosed().subscribe(
      (selected)=>{
        if(selected != null) {
          this.selectedShippingMethod=selected
          this.checkoutSvc.setShippingMethod(selected.code).subscribe((res)=>{
            if(res.message_code == 200){
              this.isStatisfied.emit('true')
              this.calc.changeShipping(this.selectedShippingMethod.price)
              this.snackbar.open(
                res.message_dialog,
                null,
                createSnackBarConfig('toast_success')
              )
            }
          })
        }
      }
    )
  }

}
