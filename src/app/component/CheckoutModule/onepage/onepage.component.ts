import { Component, OnInit } from '@angular/core';
import { CheckoutService, Quote } from 'src/app/services/checkout/checkout.service';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { Router } from '@angular/router';
import { Calculation } from 'src/app/services/calculation';

import {faChevronRight,faLock} from '@fortawesome/free-solid-svg-icons';
import { Item } from 'src/app/model/item';
import { TagService } from 'src/app/services/tag/tag.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';

@Component({
  selector: 'app-onepage',
  templateUrl: './onepage.component.html',
  styleUrls: ['./onepage.component.scss']
})

export class OnepageComponent implements OnInit {
  items: Item[]
  quote: Quote

  wantsToSendAsGift: Boolean
  
  isShippingAddressValid: Boolean
  isShippingMethodValid: Boolean
  isSendAsGiftValid: Boolean

  isProcessingOrder: Boolean
  isLoading: Boolean

  icon 
  icon_lock
  
  public calc = Calculation

  giftForm = new FormGroup({
    recipient: new FormControl(null),
    message: new FormControl(null),
  })
  
  constructor(
    private checkoutSvc: CheckoutService,
    public core: CoreServiceService,
    private router: Router,
    private tagSvc: TagService,
    private snackBar: MatSnackBar
    ) {}

  
  ngOnInit() {
    this.wantsToSendAsGift=false;
    this.isSendAsGiftValid=false;
    
    this.isProcessingOrder=false;
    this.isLoading=true;

    this.icon = faChevronRight;
    this.icon_lock = faLock;

    this.refresh()
  }

  refresh(){
    this.checkoutSvc.getQuote().subscribe(
      (response)=>{
        console.log(response)
        this.isLoading=false
        this.quote=response

        this.calc.subtotal = this.quote.subtotal
        this.calc.promo = this.quote.promo
        this.calc.total = this.quote.total

        this.items=response.quote

        if(response.quote.some(item => item.quantity > item.product_quantity)){
          this.router.navigate(['/','cart'])
        }
      }
    )
  }

  isShippingAddressValidListener(e: String){
    if(e == 'true') this.isShippingAddressValid = true;
    else if(e == 'true_changed'){
      this.isShippingAddressValid = true;
      this.isShippingMethodValid = false;
    }
    else if(e == 'false') this.isShippingAddressValid = false;
    this.refresh()
  }

  isShippingMethodValidListener(e: String){
    if(e == 'true') this.isShippingMethodValid = true;
    else if(e == 'false') this.isShippingMethodValid = false;
    this.refresh()
  }

  sendAsGiftDataListener(e:any){
    this.wantsToSendAsGift=e.enabled
    this.isSendAsGiftValid=e.valid
    this.giftForm.patchValue(e)
  }

  createOrder(){
    this.isProcessingOrder=true
    this.checkoutSvc.createOrder(
      this.giftForm.get('recipient').value,
      this.giftForm.get('message').value
    ).subscribe(
      (response)=>{
        console.log(response)
        if(response.message_code == 200){
          if(response.payment.hasOwnProperty('token')){
            if(response.payment.token != undefined){
              this.tagSvc.beginPayment(this.items)
              this.router.navigate(['/','payment'],
              { queryParams: {
                token: response.payment.token,
                order_id: response.order_id} }
              )
              return
            }
            location.href = "https://wia.id/sales/order/view/order_id/"+response.order_id
          }
          location.href = "https://wia.id/sales/order/view/order_id/"+response.order_id
        }else{
          this.snackBar.open(response.message_dialog,null,createSnackBarConfig("toast_danger"))
          this.isProcessingOrder=false
          this.router.navigate(['/','cart'])
        }
      }
    )
  }

  interact(e: String){
    if(e.match('leading')){
      this.router.navigate(['/','cart'])
    }else if(e.match('title')){

    }else if(e.match('trailing')){
      this.openLiveChat()
    }
  }

  openLiveChat(){
  }

}
