import { Component, OnInit, ViewEncapsulation, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { Address, isAddressSame } from 'src/app/model/address';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { EditAddressComponent } from '../edit-address/edit-address.component';
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';

@Component({
  selector: 'app-select-shipping-address',
  templateUrl: './select-shipping-address.component.html',
  styleUrls: ['./select-shipping-address.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectShippingAddressComponent implements OnInit, OnDestroy{

  ngOnInit(): void {
    this.core.disableScrollBody();
  }
  
  ngOnDestroy(): void {
    this.core.enableScrollBody();
  }

  addresses: Address[];
  
  selectedAddressIndex: any
  
  constructor(
    private checkoutSvc: CheckoutService, 
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SelectShippingAddressComponent>,
    private snackBar: MatSnackBar,
    private matSnackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public selectedAddress: Address,

    public core: CoreServiceService) {
      this.addresses=[]
      this.dialogRef.afterOpened().subscribe(()=>{
        this.refresh()
      })
    }

  refresh() {
    this.addresses=null;
    this.checkoutSvc.getAddresses().subscribe((f)=>{
      this.addresses = f.address_list;
      if(this.addresses == null){
        this.matSnackbar.open("You don't have another address(es), please add one",null,createSnackBarConfig("toast_warning"))
        this.dialogRef.close()
        return
      }
      
      for(let n = 0; n < this.addresses.length; n++){
        if(isAddressSame(this.addresses[n],this.selectedAddress)){
          this.selectedAddressIndex = n
          break;
        }
      }
      
    })
  }

  interact(e: string){
    if(e.match('trailing')){
      this.dialogRef.close({
        address: this.addresses[this.selectedAddressIndex]
      });
    }
  }

  useThisAddress(index){
    this.checkoutSvc.setSelectedAddress(this.addresses[index]).subscribe(
      (response)=>{
        if(response.message_code == 200){
          this.snackBar.open(response.message_dialog,null,
            createSnackBarConfig('toast_success')
          );
          this.dialogRef.close({
            address: this.addresses[index]
          });
        }else{
          this.snackBar.open(response.message_dialog,null,
          createSnackBarConfig('toast_danger')
        );
        }
    })
  }

  editThisAddress(index){
    let dia = this.dialog.open(EditAddressComponent, this.core.dialogConfig(
      this.addresses[index]
    ))
    
    dia.afterClosed().subscribe((response)=>{
      if(response != null){
        if(response.refresh) this.refresh()
      }
    })
  }

}
