import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { AddPinpointComponent } from '../add-address/add-pinpoint/add-pinpoint.component';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { Region } from 'src/app/model/region';
import { City } from 'src/app/model/city';
import { Address } from 'src/app/model/address';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.scss']
})

export class EditAddressComponent implements OnInit, OnDestroy {
  isProcessing: boolean

  regions: Region[]
  cities: City[]

  selectedRegion: Region
  selectedCity: City

  editFormCtrl = new FormGroup({
    id: new FormControl(null),
    location: new FormControl(null),
    street: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    postcode: new FormControl(null),
    region: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    city: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    telephone: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    lat: new FormControl(null),
    lon: new FormControl(null)
  })

  constructor(
    public snackBar: MatSnackBar,
    public dia: MatDialog,
    public checkoutSvc: CheckoutService,
    public dialogRef: MatDialogRef<EditAddressComponent>,
    public core: CoreServiceService,
    @Inject(MAT_DIALOG_DATA) public data: Address
  ) { }

  ngOnInit() {
    this.regions=[]
    this.cities=[]
    this.isProcessing=false;
    this.core.disableScrollBody();

    if(this.data != null){
      this.editFormCtrl.patchValue(this.data)

      //is there any coordinates?
      if(this.data.vat_id != null){
        var temLatLon=this.data.vat_id.split(',')
        this.editFormCtrl.get('lat').setValue(temLatLon[0])
        this.editFormCtrl.get('lon').setValue(temLatLon[1])
      }
    }

    this.checkoutSvc.getRegions('ID').subscribe(
      (response)=>{
        if(response.message_code == 200){
          this.regions = response.region_list
          
          if(this.data == null) return
          this.selectedRegion = this.regions.find((item)=>item.name == this.data.region)
        }else{

        }
      }
    )
  }

  updateCity(){
    if(this.selectedRegion == null) return

    this.editFormCtrl.get('city').setValue(null)
    this.cities=[]
    this.checkoutSvc.getCities(this.selectedRegion.region_id).subscribe(
      (response)=>{
        if(response.message_code == 200){
          this.cities = response.city_list

          if(this.data == null) return
          this.selectedCity = this.cities.find((item)=> item.display_name == this.data.city)
        }else{

        }
      }
    )
  }

  ngOnDestroy(): void {
    this.core.enableScrollBody();
  }

  openPinpointDialog(){
    if(this.editFormCtrl.valid){

      let dialog = this.dia.open(AddPinpointComponent, this.core.dialogConfig(
        this.editFormCtrl.value['street']+', '+this.selectedCity.display_name+
        ', '+this.selectedRegion.name+' '+this.editFormCtrl.value['postcode']
      ));
      
      dialog.afterClosed().subscribe(coordinates=>{
        if(coordinates != null){
          this.editFormCtrl.get('lat').setValue(coordinates.lat)
          this.editFormCtrl.get('lon').setValue(coordinates.lon)
        }
      })

    }else{
      this.snackBar.open('Please correct error(s) in this form to continue',
        null,
        createSnackBarConfig('toast_danger')
      );
    }
  }
  
  onSubmit(){
    if(this.editFormCtrl.valid){
      this.isProcessing=true;
      var sent: any = this.editFormCtrl.value
      
      //conforming with Magento address structure
      sent.company= "company"
      sent.country_id="ID"
      sent.street=this.editFormCtrl.get('street').value
      sent.location=this.editFormCtrl.get('location').value

      sent.region = this.editFormCtrl.value['region'].name
      sent.city = this.editFormCtrl.value['city'].display_name
      sent.region_id = this.selectedRegion.region_id
    
      //Setting up pinpoint
      if(this.editFormCtrl.get('lat').value != null &&
      this.editFormCtrl.get('lon').value != null) sent.vat_id = this.editFormCtrl.get('lat').value+','+ this.editFormCtrl.get('lon').value
        
        console.log("Before send edit address")
        console.log(sent)
        this.checkoutSvc.editAddress(sent).subscribe((response)=>{
          this.isProcessing=false;
          console.log("Reply address")
          console.log(response)
          if(response.message_code == 200){
            this.snackBar.open(response.message_dialog,
            null,
            createSnackBarConfig('toast_success')
            );
            this.dialogRef.close({
              refresh:true
            });
          }else{
            this.snackBar.open(response.message_dialog,
              null,
              createSnackBarConfig('toast_danger')
            );
          }
        })
    }else{
      this.snackBar.open("Please correct error(s) in this form to continue",
          null,
          createSnackBarConfig('toast_danger')
        );
    }
  }

  interact(e: string){
    if(e.match('title')){
      
    }else if(e.match('trailing')){
      this.dialogRef.close({
        'refresh':false
      });
    }
  }
}
