import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPinpointComponent } from './add-pinpoint.component';

describe('AddPinpointComponent', () => {
  let component: AddPinpointComponent;
  let fixture: ComponentFixture<AddPinpointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPinpointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPinpointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
