import { Component, OnInit, NgZone, ElementRef, ViewChild, Input, Inject } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-add-pinpoint',
  templateUrl: './add-pinpoint.component.html',
  styleUrls: ['./add-pinpoint.component.scss']
})
export class AddPinpointComponent implements OnInit {
  isValid: Boolean;
  selectedLatitude: Number;
  selectedLongitude: Number;

  mapLatitude: Number;
  mapLongitude: Number

  zoom = 15;
  private geoCoder;

  place: String;
  
  constructor(
    private ngZone: NgZone,
    private mapsAPILoader: MapsAPILoader,
    private dialogRef: MatDialogRef<AddPinpointComponent>,
    @Inject(MAT_DIALOG_DATA) private inputAddress: String) {
      this.place = this.inputAddress;
    }

  ngOnInit() {
    this.isValid = false

    //put map and pin to WIA HQ first
    this.mapLatitude =  -6.175387099999986;
    this.mapLongitude = 106.82714748255614;

    this.ngZone.run(()=>{
      this.mapsAPILoader.load().then(() => {   
        this.geoCoder = new google.maps.Geocoder;
          this.geoCoder.geocode({ 'address': this.place }, (results, status) => {
            if (status === 'OK') {
              if (results[0]) {
                this.ngZone.run(()=>{
                  this.mapsAPILoader.load().then(()=>{
                    this.isValid = true
                    this.selectedLatitude=results[0].geometry.location.lat()
                    this.selectedLongitude=results[0].geometry.location.lng()
        
                    this.mapLatitude=results[0].geometry.location.lat()
                    this.mapLongitude=results[0].geometry.location.lng()
        
                    this.place = results[0].formatted_address
                  })
                })
              } else {
                window.alert('No results found');
              }
            } else {
              window.alert('Geocoder failed due to: ' + status)
            }
          })

      });
    })
  }

  markerDragEnd(event: MouseEvent) {
    this.selectedLatitude = event.coords.lat;
    this.selectedLongitude = event.coords.lng;
    this.getAddress(this.selectedLatitude, this.selectedLongitude);
  }


  setCurrentLocation(){
    this.ngOnInit();
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.selectedLatitude = position.coords.latitude;
        this.selectedLongitude = position.coords.longitude
        this.mapLatitude = this.selectedLatitude
        this.mapLongitude =   this.selectedLongitude
        this.getAddress(this.selectedLatitude, this.selectedLongitude);
      });
    }
  }

  getAddress(latitude, longitude) {
    this.ngZone.run(()=>{
      this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.place = results[0].formatted_address
          } else {
           // window.alert('No results found');
          }
        } else {
          //window.alert('Geocoder failed due to: ' + status)
        }
   
      }); 
    })
  }

  interact(e: String){
    if(e == 'leading') this.dialogRef.close();
  }

  confirmPinpoint(){
    this.dialogRef.close({
      lat: this.selectedLatitude,
      lon: this.selectedLongitude
    });
  }
}
