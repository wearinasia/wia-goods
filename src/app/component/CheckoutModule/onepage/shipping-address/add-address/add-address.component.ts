import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { createSnackBarConfig } from 'src/app/shared/snackbar/snackbar';
import { AddPinpointComponent } from './add-pinpoint/add-pinpoint.component';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { Region } from 'src/app/model/region';
import { City } from 'src/app/model/city';
import { Address } from 'src/app/model/address';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.scss']
})

export class AddAddressComponent implements OnInit, OnDestroy{
  isProcessing: boolean

  regions: Region[]
  cities: City[]

  selectedRegion: Region
  selectedCity: City

  addFormCtrl = new FormGroup({
    id: new FormControl(null),
    location: new FormControl(null),
    street: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    postcode: new FormControl(null),
    region: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    city: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    telephone: new FormControl(null,{
      updateOn: 'change',
      validators: [Validators.required]
    }),
    lat: new FormControl(null),
    lon: new FormControl(null)
  })

  constructor(
    public snackBar: MatSnackBar,
    public dia: MatDialog,
    public checkoutSvc: CheckoutService,
    public dialogRef: MatDialogRef<AddAddressComponent>,
    public core: CoreServiceService,
    @Inject(MAT_DIALOG_DATA) public data: Address
  ) { }

  ngOnInit() {
    this.regions=[]
    this.cities=[]
    this.isProcessing=false;
    this.core.disableScrollBody();

    if(this.data != null){
      this.addFormCtrl.patchValue(this.data)

      //is there any coordinates?
      if(this.data.vat_id != null){
        var temLatLon=this.data.vat_id.split(',')
        this.addFormCtrl.get('lat').setValue(temLatLon[0])
        this.addFormCtrl.get('lon').setValue(temLatLon[1])
      }
    }
    
    this.checkoutSvc.getRegions('ID').subscribe(
      (response)=>{
        if(response.message_code == 200){
          this.regions = response.region_list

          if(this.data == null) return
          this.selectedRegion = this.regions.find((item)=>item.name == this.data.region)
        }else{

        }
      }
    )
  }

  updateCity(){
    if(this.selectedRegion == null) return

    this.addFormCtrl.get('city').setValue(null)
    this.cities=[]
    this.checkoutSvc.getCities(this.selectedRegion.region_id).subscribe(
      (response)=>{
        if(response.message_code == 200){
          this.cities = response.city_list

          if(this.data == null) return
          this.selectedCity = this.cities.find((item)=> item.display_name == this.data.city)
        }else{

        }
      }
    )
  }

  ngOnDestroy(): void {
    this.core.enableScrollBody();
  }

  openPinpointDialog(){
    if(this.addFormCtrl.valid){
      
      let dialog = this.dia.open(AddPinpointComponent, this.core.dialogConfig(
        this.addFormCtrl.value['street']+', '+this.addFormCtrl.value['city'].display_name+
        ', '+this.addFormCtrl.value['region'].name+' '+this.addFormCtrl.value['postcode']
      ));
      
      dialog.afterClosed().subscribe(coordinates=>{
        if(coordinates != null){
          this.addFormCtrl.get('lat').setValue(coordinates.lat)
          this.addFormCtrl.get('lon').setValue(coordinates.lon)
        }
      })

    }else{
      this.snackBar.open('Please correct error(s) in this form to continue',
        null,
        createSnackBarConfig('toast_danger')
      );
    }
  }
  
  onSubmit(){
    if(this.addFormCtrl.valid){
      this.isProcessing=true;
      var sent: any = this.addFormCtrl.value
      
      //conforming with Magento address structure
      sent.company= "company"
      sent.country_id="ID"
      sent.street = this.addFormCtrl.get('street').value
      sent.location = this.addFormCtrl.get('location').value

      sent.region = this.addFormCtrl.value['region'].name
      sent.city = this.addFormCtrl.value['city'].display_name
      sent.region_id = this.selectedRegion.region_id

      //Setting up pinpoint
      if(this.addFormCtrl.get('lat').value != null &&
      this.addFormCtrl.get('lon').value != null) sent.vat_id = this.addFormCtrl.get('lat').value+','+ this.addFormCtrl.get('lon').value

      this.checkoutSvc.addNewAddress(sent).subscribe((response)=>{
        this.isProcessing=false;
        if(response.message_code == 200){
          this.snackBar.open(response.message_dialog,
          null,
          createSnackBarConfig('toast_success')
          );
          this.dialogRef.close({
            refresh:true
          });
        }else{
          this.snackBar.open(response.message_dialog,
            null,
            createSnackBarConfig('toast_danger')
          );
        }
      })
    }else{
      this.snackBar.open("Please correct error(s) in this form to continue",
          null,
          createSnackBarConfig('toast_danger')
        );
    }
  }

  interact(e: string){
    if(e.match('title')){
      
    }else if(e.match('trailing')){
      this.dialogRef.close({
        refresh:false
      });
    }
  }
}
