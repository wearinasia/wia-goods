import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { CheckoutService, Quote } from 'src/app/services/checkout/checkout.service';
import { Address, isAddressSame } from 'src/app/model/address';
import { MatDialog } from '@angular/material';
import { SelectShippingAddressComponent } from './select-shipping-address/select-shipping-address.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { CoreServiceService } from 'src/app/services/core/core-service.service';

@Component({
  selector: 'app-shipping-address',
  templateUrl: './shipping-address.component.html',
  styleUrls: ['./shipping-address.component.scss']
})
export class ShippingAddressComponent implements OnInit, OnChanges{

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if(changes.hasOwnProperty('quote')){
      if(JSON.stringify(changes.quote.previousValue) != JSON.stringify(changes.quote.currentValue)) this.ngOnInit()
    }
  }

  @Input() quote: Quote
  @Output() isStatisfied = new EventEmitter()
  
  selectedAddress: Address;
  hasAddress: Boolean
  isLoading: Boolean

  constructor(
    private checkoutSvc: CheckoutService,
    private dia: MatDialog,
    private core: CoreServiceService,
    ) { }

  ngOnInit() {
    console.log("Shipping address initializing!")
    this.hasAddress=true
    this.isLoading=false
    this.selectedAddress = this.quote.address;
    console.log(this.selectedAddress)
    //tell the main controller
    if(this.selectedAddress == null){
      this.hasAddress=false
      this.isStatisfied.emit('false')
    }
    else this.isStatisfied.emit('true')
  }

  //see if there is difference with existing and selected address
  openSelectShippingAddress(){
    let dialog = this.dia.open(SelectShippingAddressComponent, this.core.dialogConfig(
      this.selectedAddress
    ));
    dialog.afterClosed().subscribe(async res=>{
      if(res != null){
        if(res.address == null) return
        if(!isAddressSame(this.selectedAddress,res.address)){
          this.isStatisfied.emit('false')
          this.checkoutSvc.setSelectedAddress(res.address).subscribe(
            (res)=>{
              if(res.message_code == 200){
                this.isStatisfied.emit('true_changed')
                this.selectedAddress=res.address
              }
            }
          )
        }
      }
    })
  }

  openAddNewAddress(){
    let dialog = this.dia.open(AddAddressComponent, this.core.dialogConfig());
    
    //see if there is add new address
    dialog.afterClosed().subscribe(res=>{
      if(res != null){
        if(res.refresh){
          //adding new address also set that new address
          //as selected shipping
          this.isStatisfied.emit('true_changed')
        }
      }
    })
  }


}
