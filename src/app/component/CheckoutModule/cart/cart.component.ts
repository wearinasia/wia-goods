import { Component, OnInit } from '@angular/core';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { CheckoutService, Quote } from 'src/app/services/checkout/checkout.service';
import { Item } from 'src/app/model/item';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  subtotal: any
  shipping: any
  promo: any
  grandtotal: any
  
  items: Item[]

  isLoading: Boolean
  isCheckoutValid: Boolean

  constructor(
    public core: CoreServiceService,
    private checkoutSvc: CheckoutService
  ) {
    this.subtotal = 0
    this.shipping = 0
    this.promo =  0
    this.grandtotal = 0
    this.items =[]
  }

  ngOnInit(keepDOM?: Boolean) {
    if(!keepDOM) this.isLoading=true
    if(!keepDOM) this.isCheckoutValid=true
    this.checkoutSvc.getQuote().subscribe((response: Quote)=>{
      this.isLoading=false
      if(response.message_code == 200){
        console.log(response)
        /*
          Let's check the item stock
        */
        this.isCheckoutValid = response.quote.every(item =>item.quantity <= item.product_quantity)

        this.subtotal = response.subtotal
        this.shipping = response.shipping == null ? 0 : response.shipping
        this.promo = response.promo
        this.grandtotal = response.total
        this.items = response.quote
      }else{
        this.subtotal = 0
        this.shipping = 0
        this.promo =  0
        this.grandtotal = 0
        this.items=[]
        //location.href="https://wia.id/goods"
      }
    })
  }

  interact(e: String){
    if(e.match('leading')){
      location.href="https://wia.id/"
    }
    if(e.match('trailing')){
      this.openLiveChat()
    }
  }

  openLiveChat(){
  }
  
  onUpdate(e){
    if(e == 'update') this.ngOnInit(true)
  }



}
