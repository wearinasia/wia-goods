import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantityBufferComponent } from './quantity-buffer.component';

describe('QuantityBufferComponent', () => {
  let component: QuantityBufferComponent;
  let fixture: ComponentFixture<QuantityBufferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuantityBufferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantityBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
