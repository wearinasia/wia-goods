import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from 'src/app/model/item';
import { CoreServiceService } from 'src/app/services/core/core-service.service';
import { CartService } from 'src/app/services/cart/cart.service';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss']
})
export class CartListComponent implements OnInit {
  @Input() items: Item[]
  @Output() onUpdate = new EventEmitter()

  selectedItem: Item
  selectedQty: any
  isProcessing: Boolean

  constructor(
    public core: CoreServiceService,
    private cartSvc: CartService,
  ) { }

  ngOnInit() {
    this.isProcessing=false
  }

  edit(i: any){
    this.core.disableScrollBody()
    this.selectedItem = this.items[i]
    this.selectedQty=this.selectedItem.quantity
    console.log(this.selectedItem)
  }
  
  remove(){
    this.isProcessing=true
    this.cartSvc.removeItem(this.selectedItem.id).subscribe((response)=>{
      this.isProcessing=false
      if(response.message_code == 200) {
        console.log(response)
        this.onUpdate.emit('update')
        this.close()
      }
    })
  }

  finishEdit(){
    this.isProcessing=true
    this.cartSvc.update(this.selectedItem.id, this.selectedQty).subscribe(
      (response)=>{
        this.isProcessing=false
        if(response.message_code == 200){
          console.log(response)
          this.onUpdate.emit('update')
          this.close()
        }
      }
    )
  }

  close(){
    this.core.enableScrollBody()
    this.selectedItem=null
  }

  add(){
    if(this.selectedQty < this.selectedItem.product_quantity) this.selectedQty+=1
  }

  subtract(){
    if(this.selectedQty > 0) this.selectedQty-=1
  }
}
