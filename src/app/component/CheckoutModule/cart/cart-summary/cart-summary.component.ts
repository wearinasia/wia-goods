import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-cart-summary',
  templateUrl: './cart-summary.component.html',
  styleUrls: ['./cart-summary.component.scss']
})
export class CartSummaryComponent implements OnInit,OnChanges {
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(changes)
    console.log(this.checkoutOk)
  }
  @Input() subtotal: any
  @Input() shipping: any
  @Input() promo: any
  @Input() grandtotal: any
  @Input() checkoutOk: Boolean
  

  constructor(
  ) {}

  ngOnInit() {
  }

}
