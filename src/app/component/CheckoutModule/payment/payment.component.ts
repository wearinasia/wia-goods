import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { TagService } from 'src/app/services/tag/tag.service';
declare var snap: any

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  order_id: String
  token: String
  isValid: Boolean
  constructor(
    private route: ActivatedRoute,
    private tagSvc: TagService
  ) { }

  ngOnInit() {
    this.isValid = true
    this.route.queryParams.subscribe(params => {
      this.token = params["token"]
      this.order_id = params['order_id']
      if(this.token != null && this.order_id != null){
        this.pay()
      }else{
        this.isValid = false;
        setTimeout(()=>{
          location.href="https://wia.id/sales/order/history/"
        }, 2000)
      }
    })
  }

  pay(){
    snap.pay(this.token,{
      onSuccess: (result)=>{
        console.log('success')
        console.log(result)
        location.href="https://wia.id/sales/order/view/order_id/"+this.order_id+"?success=true"
      },
      onPending: (result)=>{
        console.log('pending');
        console.log(result)
        location.href="https://wia.id/sales/order/view/order_id/"+this.order_id
      },
      onError: (result)=>{
        console.log('error');
        console.log(result)
        location.href="https://wia.id/sales/order/view/order_id/"+this.order_id
      },
      onClose: ()=>{
        location.href="https://wia.id/sales/order/view/order_id/"+this.order_id
        console.log('customer closed the popup without finishing the payment');
      }
    })
  }

}
