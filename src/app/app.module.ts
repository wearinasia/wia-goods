import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OnepageComponent } from './component/CheckoutModule/onepage/onepage.component';

import { DeviceDetectorModule } from 'ngx-device-detector';
import { ShippingMethodComponent } from './component/CheckoutModule/onepage/shipping-method/shipping-method.component';
import { ShippingAddressComponent } from './component/CheckoutModule/onepage/shipping-address/shipping-address.component';
import { SelectShippingMethodComponent } from './component/CheckoutModule/onepage/shipping-method/select-shipping-method/select-shipping-method.component';
import { MatDialogModule, MatDialogRef, MatFormFieldModule, MatInputModule, MatSnackBarModule, MatSelectModule, MatSlideToggleModule, MatNativeDateModule, MatRadioModule } from '@angular/material';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { SelectShippingAddressComponent } from './component/CheckoutModule/onepage/shipping-address/select-shipping-address/select-shipping-address.component';
import { AddAddressComponent } from './component/CheckoutModule/onepage/shipping-address/add-address/add-address.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddPinpointComponent } from './component/CheckoutModule/onepage/shipping-address/add-address/add-pinpoint/add-pinpoint.component';

import { AgmCoreModule } from '@agm/core';
import { SummaryComponent } from './component/CheckoutModule/onepage/summary/summary-desktop/summary.component';
import { SelectPromoComponent } from './component/CheckoutModule/onepage/promo/select-promo/select-promo.component';
import { PromoDetailsComponent } from './component/CheckoutModule/onepage/promo/select-promo/promo-details/promo-details.component';
import { AccountComponent } from './component/CheckoutModule/onepage/account/account.component';
import { MobileHeaderComponent } from './shared/header/mobile-header/mobile-header.component';
import { PromoComponent } from './component/CheckoutModule/onepage/promo/promo-desktop/promo.component';
import { PromoMobileComponent } from './component/CheckoutModule/onepage/promo/promo-mobile/promo-mobile.component';
import { SummaryMobileComponent } from './component/CheckoutModule/onepage/summary/summary-mobile/summary-mobile.component';
import { SummaryDetailComponent } from './component/CheckoutModule/onepage/summary/summary-mobile/summary-detail/summary-detail.component';
import { EditAddressComponent } from './component/CheckoutModule/onepage/shipping-address/edit-address/edit-address.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PaymentComponent } from './component/CheckoutModule/payment/payment.component';
import { CartComponent } from './component/CheckoutModule/cart/cart.component';
import { CartSummaryComponent } from './component/CheckoutModule/cart/cart-summary/cart-summary.component';
import { CartListComponent } from './component/CheckoutModule/cart/cart-list/cart-list.component';
import { QuantityBufferComponent } from './component/CheckoutModule/cart/cart-list/quantity-buffer/quantity-buffer.component';
import { SendAsGiftComponent } from './component/CheckoutModule/onepage/send-as-gift/send-as-gift.component';
import { InquiryFormComponent } from './component/BusinessInquiryModule/inquiry-form/inquiry-form.component';
import { BusinessLandingPageComponent } from './component/BusinessInquiryModule/business-landing-page/business-landing-page.component';

import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  declarations: [
    AppComponent,
    ShippingMethodComponent,
    PaymentComponent,
    OnepageComponent,
    ShippingAddressComponent,
    SelectShippingMethodComponent,
    SelectShippingAddressComponent,
    AddAddressComponent,
    AddPinpointComponent,
    SummaryComponent,
    SelectPromoComponent,
    PromoDetailsComponent,
    PromoComponent,
    AccountComponent,
    MobileHeaderComponent,
    PromoMobileComponent,
    SummaryMobileComponent,
    SummaryDetailComponent,
    EditAddressComponent,
    CartComponent,
    CartSummaryComponent,
    CartListComponent,
    QuantityBufferComponent,
    SendAsGiftComponent,
    InquiryFormComponent,
    BusinessLandingPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DeviceDetectorModule.forRoot(),
    MatDialogModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatSelectModule,
    FontAwesomeModule,
    MatSlideToggleModule,
    MatRadioModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAmlnIAu52yOhMzIPFGxSsDaHB9TXmqhQk',
      libraries: ['places']
    }),
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents:[
    SelectShippingMethodComponent,
    SelectShippingAddressComponent,
    AddAddressComponent,
    AddPinpointComponent,
    SelectPromoComponent,
    PromoDetailsComponent,
    SummaryDetailComponent,
    EditAddressComponent,
    QuantityBufferComponent
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {}
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
