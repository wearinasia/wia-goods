import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {
  faArrowLeft, faChevronLeft,
  faArrowRight, faChevronRight,
  faHamburger,
  faChevronUp,
  faChevronDown, 
  IconDefinition} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.scss']
})


export class MobileHeaderComponent implements OnInit{
  faLeading: IconDefinition
  faTitle: IconDefinition
  faTrailing: IconDefinition

  faConversion={
    'arrowLeft': faArrowLeft,
    'arrowRight': faArrowRight,
    'chevronLeft': faChevronLeft,
    'chevronRight': faChevronRight,
    'chevronUp': faChevronUp,
    'chevronDown': faChevronDown,
    'hamburger': faHamburger,
  }

  @Input() leading: string
  @Output() interaction = new EventEmitter()
  @Input() title: string
  @Input() trailing: string

  @Input() leadingIcon: string
  @Input() titleIcon: string
  @Input() trailingIcon: string

  constructor() {
  }

  ngOnInit(): void {
    this.faLeading = this.faConversion[this.leadingIcon]
    this.faTitle = this.faConversion[this.titleIcon]
    this.faTrailing = this.faConversion[this.trailingIcon]
  }

}
