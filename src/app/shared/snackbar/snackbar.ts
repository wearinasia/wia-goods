import { MatSnackBarConfig } from "@angular/material";

type className='toast_primary'|'toast_info'|'toast_warning'|'toast_danger'|'toast_success';

export function createSnackBarConfig(className?: className, duration? : number){
    var config = new MatSnackBarConfig();
    config.verticalPosition = 'bottom';
    config.horizontalPosition = 'center';
    config.duration = duration == null? 2000 : duration
    config.panelClass = [className == null ? 'toast_primary' : className]
    config.politeness = "off"
    config.verticalPosition="top"
    return config;
}